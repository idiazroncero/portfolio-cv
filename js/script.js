$(function(){

	// -------------------------------
	// 1 - VARS Y CONFIGURACION INICIAL
	// -------------------------------

	// Añadimos dinámicamente la clase. No queremos que cualquier error de JS
	// deje la web oculta.
	$('.column-header').addClass('init');
	$('.column-nav').addClass('init');
	$('.column-main').addClass('init');
	$('.secondary-menu').hide();

	// Listado de miniyos a rotar
	var miniyos = ['images/miniyo.jpg','images/miniyo2.jpg', 'images/miniyo.gif'];
	// Utilizada para memorizar el último slide activo antes de secondary
	var lastSlide = 0, carrusel, carruselNav, carrusel_data;
	// ¿Se ha compartido una URL con anchor?
	var hash = window.location.hash;
	// Ajustamos la sensibilidad de Flickity en su prototipo
	// como recomendado en https://github.com/metafizzy/flickity/issues/138
	Flickity.prototype.hasDragStarted = function( moveVector ) {
	  return !this.isTouchScrolling && Math.abs( moveVector.x ) > 150;
	};

	// -------------------------------
	// 2 - FUNCIONES
	// -------------------------------

	var setMiniyo = function(){ // Pone un Miniyo aleatorio
		var random = Math.floor(Math.random() * miniyos.length);
		$('.miniyo img').attr('src', miniyos[random]);
		if(random === 2) {
			$('.miniyo').addClass('miniyo--square');
		}
	};

	var showSecondary = function(item){ // Muestra el menú secundario indicado en ítem
		// Cierra cualquiera que pudiera estar abierto.
		$('.secondary-menu').hide();
		// Encuentra el menú secundario correspondiente a través de href
		var href = $(item).attr('href');
		// Muestra el elemento html que contiene el menú
		$(href).show();
		// ...y mueve la columna entera con animación css para hacerlo visible
		$('.column-nav').addClass('secondary-on');
		// Si el menú esta en modo horizontal, recalcula la altura
		if ($(window).width() > 480 && $(window).width() < 968) {
			$('#main-menu').animate({
				minHeight : $(href).height()
			}, 400);
		}
	};

	var hideSecondary = function(){ // Esconde el menú secundario
		// Devuelve la columna a su estado
		$('.column-nav').removeClass('secondary-on');
		setTimeout(function(){
			// y, en un tiempo identico al transition css, retira la visibilidad a todo menú secundario
			$('.secondary-menu').hide();
		}, 500);
		// Si el menú esta en modo horizontal, recalcula la altura
		if ($(window).width() > 480 && $(window).width() < 968) {
			$('#main-menu').css('min-height', 'auto');
		}
	};

	var itemActivo = function(item){ // Hace activo el ítem de menú actual
		// Borra todos los existentes
		$('.menu-item').removeClass('menu-item--active');
		// Y añade la clase sólo al actual
		$(item).parent().addClass('menu-item--active');
	};

	var setZoom = function(){ // Activa jquery.zoom
		$('.zoomable').zoom({
			on: 'click',
			magnify: '1',
			onZoomIn: function(){
				$(this).parent().css('cursor', 'zoom-out');
				$(this).css('cursor', 'zoom-out');
			},
			onZoomOut: function(){
				$(this).parent().css('cursor', 'zoom-in');
				$(this).css('cursor', 'zoom-in');
			},
		});
	};

	var getSlideIndex = function(item){ // Obtiene el index de la diapo correspondiente al ítem
		var index = $('.carrusel section').index($(item.attr('href')));
		return index;
	};

	var getData = function(url){
		$.ajax({
			url: url,
			method: 'GET',
			error:function(jq, data, exc){
				return false;
			},
			success: function(data, status){
				return data;
			}
		});
	}

	var getVersion = function(){ // Request la última tag vía Ajax
		$.ajax({
			url: 'https://api.bitbucket.org/2.0/repositories/idiazroncero/portfolio-cv/refs/tags',
			method: 'GET',
			error:function(jq, data, exc){
				console.log(data);
				console.log(exc);
			},
			success: function(data, status){
				var datosReales = data;
				console.log(data);
				if (data.next) {
					datosReales = getData(data.next);
				}
				console.log(datosReales);
				var lastTag = datosReales.values[datosReales.values.length - 1];
				$('.version').html('<a href="' + lastTag.links.html.href + '" target="_blank">' + lastTag.name + '</a>');
			}
		});
	};

	var shuffleToolbox = function () { // Extraído de http://jsfiddle.net/C6LPY/2/
	    var parent = $(".toolbox");
	    var divs = parent.children();
	    while (divs.length) {
	        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	    }
	};

	// var gitEvents = function(){ // AJAX requests a los endpoints de git y bitbucket
	// 	$.ajax({
	// 		accepts: {
	// 			github: 'application/vnd.github.v3+json'
	// 		},
	// 		url: 'https://api.github.com/users/idiazroncero/events',
	// 		method: 'GET',
	// 		dataType: "json",
	// 		success: function(data, status){
	// 			for (i = 0; i < data.length; i++) {
	// 				var who = data[i].actor.login;
	// 				var who_url = data[i].actor.url;
	// 				var raw_what = data[i].type;
	// 				console.log(data[i]);
	// 				switch (raw_what) {
	// 					case ('WatchEvent'):
	// 						var what = "añadió una estrella a";
	// 						break;
	// 					case ('PushEvent'):
	// 						var what = "envió un commit a";
	// 						break;
	// 					case ('CreateEvent'):
	// 						var what = "creó";
	// 						break;
	// 					case ('DeleteEvent'):
	// 						var what = "borró";
	// 						break;
	// 					case ('FollowEvent'):
	// 						var what = "siguió a";
	// 						break;
	// 					case ('DeleteEvent'):
	// 						var what = "borró";
	// 						break;
	// 					case ('IssueCommentEvent'):
	// 						var what = "comentó una issue en";
	// 						break;
	// 					default:
	// 						var what = raw_what
	// 						break;
	// 				}
	// 				var where = data[i].repo.name;
	// 				var where_url = data[i].repo.url;
	// 				var html = 	'<li><a href="' +
	// 							who_url +
	// 							'" target="_blank">' +
	// 							who + '</a> ' + what + ' ' +
	// 							'<a href="' +
	// 							where_url +
	// 							'" target="_blank">' +
	// 							where + '</li>'
	// 							;
	// 				$('#gitfeed').append(html);
	// 			};
	// 		}
	// 	});
	// 	// Necesitaremos montar algo server-side (node.js con express?) para esto
	// 	// mientras tanto, hacemos la chapuza de meter el token a mano
	// 	$.ajax({
	// 		url: 'https://bitbucket.org/idiazroncero/rss/feed',
	// 		method: 'GET',
	// 		data: {
	// 			token: '8b353caa4415858f22049446a9a79f8d'
	// 		},
	// 		error:function(jq, data, exc){
	// 			console.log(data);
	// 			console.log(exc);
	// 		},
	// 		success: function(data, status){
	// 			console.log(data);
	// 			$(data).find('item').each(function(){
	// 				// var title = $(this).find('title').text();
	// 				// var hour = $(this).find('pubDate').text();
	// 				var text = $(this).find('description').text();
	// 				html = '<li>' + text + '</li>';
	// 				$('#bitfeed').append(html);
	// 			})
	// 		}
	// 	});
	// }

	var setList = function(){
		var userList = new List('toolbox__list', {
			valueNames: ['tool__tags', 'tool__name'],
			listClass: 'toolbox'
		});
	};

	var navigateToHash = function(){ // Si la URL tiene anchor, navega a la diapositiva
		if (hash) {
			var index = $(hash).index('.carrusel section');
			carrusel.flickity('select', index);
		}
	};

	var initOnLoad = function(){ 	// Tareas dependientes de window.load y no document.ready
		$('.loading').fadeOut(1000);
		$('.column-header').removeClass('init');
		$('.column-nav').removeClass('init');
		$('.column-main').removeClass('init');
		navigateToHash();
	};

	var init = function(){
		setList();
		setMiniyo();
		setZoom();
		getVersion();
		shuffleToolbox();
	};


	// -------------------------------
	// 3 - WINDOW.LOAD / CARRUSEL
	// -------------------------------
	var carrusel, carruselNav, carrusel_data;
	// Tenemos que esperar a window.load para asegurar que flickity
	// puede calcular correctamente todas las alturas, etc...
	$(window).load(function(){
		
		if($(window).width() < 480) {
			carrusel = $('.carrusel').flickity({ // Flickity en main carrusel
				cellSelector: 'section',
				prevNextButtons: false,
				pageDots:false,
				adaptiveHeight: true
				//setGallerySize: false
			});
			carrusel_data = carrusel.data('flickity'); // Lee los datos del carrusel
		} else {
			carrusel = $('.carrusel').flickity({ // Flickity en main carrusel
				cellSelector: 'section',
				prevNextButtons: false,
				pageDots:false,
				setGallerySize: false,
			});
			carrusel_data = carrusel.data('flickity'); // Lee los datos del carrusel	
		}

		carruselNav = $('.alt-nav__list').flickity({ // Crea el menu alternativo asNavFor carrusel
			asNavFor : '.carrusel',
			prevNextButtons: false,
			contain:true,
			accesibility:true,
			percentPosition: false,
			pageDots:false,
		});

		carrusel.on('cellSelect', function(){  // Actualiza el menú lateral.
			var index = carrusel_data.selectedIndex;
			var item = $('.to-diapo').eq(index);
			var isSecondary = item.parents('.secondary-menu').length > 0 ? item.parents('.secondary-menu').prev('a') : false;
			// Activa el ítem de menu correspondiente a este index
			itemActivo(item);
			// Saca el menú secundario si es necesario.
			if (isSecondary) {
				showSecondary(isSecondary);
			} else {
				hideSecondary();
			}
		});

		// En Chrome en Ubuntu, a saber por qué, el primer cálculo de flickity
		// provoca un error mínimo pero molesto de render del texto.
		// Chapucero, pero tenemos que forzar un resize en .load
		carrusel.flickity('resize');

		// Inicializador dependiente de .load
		initOnLoad();
	});


	// -------------------------------
	// 4 - EVENT LISTENERS
	// -------------------------------

	$('#main-menu li a').click(function(e){ // Determina el comportamiento de item de menu: ¿diapo o secundario?
		// Mata el href del link
		e.preventDefault();
		// Mira la clase para ver que tipo de item de menú es
		var clase = $(this).attr('class');
		var index;
		// Actúa según la clase del ítem de menú
		if (clase === 'to-diapo') {
			// Activa el ítem actual
			itemActivo(this);
			// Encuentra el index actual de este ítem
			index = $('.to-diapo').index($(this));
			// Mueve el carrusel hasta el índex correspondiente
			carrusel.flickity('select', index);
		} else if (clase === 'to-secondary') {
			// Marca lastSlide como el item que lanza el menú
			lastSlide = $('.is-selected').index('.carrusel section');
			// Muestra el menú secundario de este ítem
			showSecondary(this);
			// Encuentra el index siguiente al actual
			var firstDiapo = $($(this).attr('href')).find('.to-diapo').eq(0);
			index = $('.to-diapo').index(firstDiapo);
			// Mueve el carrusel hasta el índex correspondiente
			// Con un poco de retraso para aumentar el dinamismo
			// y hacer evidente el doble movimiento realizado
			setTimeout(function(){
				carrusel.flickity('select', index);
			}, 250);
		}
		// Por si PreventDefault falla...
		return false;
	});

	$('.to-primary').on('click', function(){ // Link que esconde el secondary menu
		// Esconde el menú secundario
		hideSecondary();
		// Muestra la última slide
		carrusel.flickity('select', lastSlide);
	});

	$('.flkty__link').click(function(e){ // Links que llevan a items de carrusel
		e.preventDefault();
		var index = getSlideIndex($(this));
		carrusel.flickity('select', index);
		return false;
	});

	$(document).keyup(function(e) { // Keypress Escape siempre sale del menú secundario
     	if (e.keyCode == 27) { // Escape
     		hideSecondary();
    	}
	});

	$('.shuffle').click(function(){
		shuffleToolbox();
	});

	// Inicializa todo lo que NO debe esperar a window.load;
	init();

});
module.exports = function(grunt) {
	grunt.initConfig({
		compass : {
			options: {
				// config: 'config.rb',
				require: 'breakpoint',
				sassDir: 'sass',
				cssDir: 'build/css',
				imagesDir: 'images'
			},
			prod : {
				options : {
					environment: 'production',
					outputStyle: 'compressed',
					trace: false,
				}
			},
			dev : {
				options : {
					environment: 'development',
					outputStyle: 'nested',
					trace: true,
				}
			}
		},

		concat: {
		  prod: {
		    src: [	'js/jquery-1.12.3.min.js',
		    		'js/flickity.pkgd.min.js',
		    		'js/list.min.js',
		    		'js/jquery.zoom.min.js',
		    		'js/script.js'
		    ],
		    dest: 'build/js/idiazroncero.js'
		  }
		},

		uglify: {
		  prod: {
		    files: {
		      'build/js/idiazroncero.min.js': ['build/js/idiazroncero.js']
		    }
		  }
		},

		jshint: {
		  // define the files to lint
		  files: ['Gruntfile.js', 'js/script.js'],
		  // configure JSHint (documented at http://www.jshint.com/docs/)
		  options: {
		    // more options here if you want to override JSHint defaults
		    globals: {
		      jQuery: true,
		      console: true,
		      module: true
		    }
		  }
		},

		postcss : {
			options : {
				map : {
					inline: false, // save all sourcemaps as separate files...
          			annotation: 'build/css/maps' // ...to the specified directory
				},
				processors: [
                    require('autoprefixer')({
                        browsers: ['> 0.1%']
                    })
                ]
			},
			prod: {
				src: 'build/css/*.css'
			},
			dev : {
				src: 'build/css/*.css'
			}
		},

	    csscomb: {
	    	comb: {
	            files: {
	            	'build/css/source/style.css' : ['css/style.css']
	            }
        	}
	    },

		includes: {
		  files: {
		    src: ['index.html'], // Source files
		    dest: 'build', // Destination directory
		    flatten: true,
		    cwd: '.',
		    options: {
		    	includePath: ['partials'],
		    }
		  }
		},

		watch : {
			compass: {
				files: ['**/*.scss', '**/*.html', '**/*.js'],
				tasks: ['default', 'jshint']
			}
		}
	}); // Init Config

	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-csscomb');
	grunt.loadNpmTasks('grunt-includes');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Carga los plugins
	grunt.registerTask('default', ['includes', 'concat:prod', 'uglify:prod', 'compass:prod', 'csscomb:comb', 'postcss:prod']);
	grunt.registerTask('dev', ['includes', 'concat:prod', 'compass:dev', 'postcss:prod']);
}; // Función wrapper